package com.lista.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lista.dao.ListaDao;
import com.lista.entity.Lista;

@RestController
@RequestMapping("listas")
public class ListaController {
	
	@Autowired
	private ListaDao listaDao;
	
	@RequestMapping(method = RequestMethod.POST)
	public Lista saveTodo(@RequestBody Lista lista){
		return listaDao.save(lista);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Lista> getAllTodo(){
		return listaDao.findAll();
	}
	
	@RequestMapping(value = "/{id}" , method = RequestMethod.GET)
	public Lista getTodo(@PathVariable("id") int id){
		return listaDao.findById(id);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public Lista updateTodo(@RequestBody Lista lista){
		return listaDao.update(lista);
	}	
	
	@RequestMapping(value = "/{id}" , method = RequestMethod.DELETE)
	public Lista deleteTodo(@PathVariable("id") int id){
		return listaDao.deleteById(id);
	}
}
