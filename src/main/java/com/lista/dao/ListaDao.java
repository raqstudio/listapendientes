package com.lista.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.lista.entity.Lista;

@Service
@Transactional
public class ListaDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public Lista save(Lista lista){
		em.persist(lista);
		return lista;
	}
	
	public List<Lista> findAll(){
		Query query = em.createQuery("select l from Lista l");
		return query.getResultList();
	}
	
	public Lista findById(int id){
		return em.find(Lista.class, id);
	}
	
	
	public Lista update(Lista lista){
		Lista l = em.find(Lista.class, lista.getId());
		l.setName(lista.getName());
		return l;
	}
	
	public Lista deleteById(int id){
		Lista lista = em.find(Lista.class, id);
		if(lista != null){
			em.remove(lista);
		}
		return lista;
	}
}
