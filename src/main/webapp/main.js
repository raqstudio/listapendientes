
var angularTodo = angular.module('angularTodo', []);

function mainController($scope, $http) {
	var apiUrl = '/lista/api/listas/';
	$scope.formData = {};

	$http.get(apiUrl)
		.success(function(data) {
			$scope.listas = data;
			console.log(data)
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});

	$scope.createTodo = function(){
		$http.post(apiUrl, $scope.formData)
			.success(function(data) {
				$scope.formData = {};
				if (data.name != null){
					$scope.listas.push(data);
					console.log(data);
				}
			})
			.error(function(data) {
				console.log('Error:' + data);
			});
	};

	$scope.deleteTodo = function(id) {
		$http.delete(apiUrl + id)
			.success(function(data) {
				findAndRemove($scope.listas, 'id', data.id);
				console.log(data);
			})
			.error(function(data) {
				console.log('Error:' + data);
			});
	};
}

function findAndRemove(array, property, value) {
	  array.forEach(function(result, index) {
	    if(result[property] === value) {
	      array.splice(index, 1);
	    }    
	  });
}
	