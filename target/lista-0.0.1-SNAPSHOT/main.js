
var angularTodo = angular.module('angularTodo', []);

function mainController($scope, $http) {
	var apiUrl = '/todo/api/todos/';
	$scope.formData = {};

	$http.get(apiUrl)
		.success(function(data) {
			$scope.todos = data;
			console.log(data)
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});

	$scope.createTodo = function(){
		$http.post(apiUrl, $scope.formData)
			.success(function(data) {
				$scope.formData = {};
				$scope.todos.push(data);
				console.log(data);
			})
			.error(function(data) {
				console.log('Error:' + data);
			});
	};

	// Borra una lista despues de checkearlo como acabado
	$scope.deleteTodo = function(id) {
		$http.delete(apiUrl + id)
			.success(function(data) {
				findAndRemove($scope.todos, 'id', data.id);
				console.log(data);
			})
			.error(function(data) {
				console.log('Error:' + data);
			});
	};
}

function findAndRemove(array, property, value) {
	  array.forEach(function(result, index) {
	    if(result[property] === value) {
	      array.splice(index, 1);
	    }    
	  });
	}
	